<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Http\Resources\Ia14Resource;
use App\Http\Resources\ShowResource;
use App\Models\Ia14;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\Array_;

class Ia14Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ia14 = Ia14::where('fecha',$id)->first();
        //$ia14 = DB::select(DB::raw("select * from ia14 where fecha ='$id'"));
        if (!$ia14){
            return response()->json(['errors' =>Array(['code' => 404, 'message'=>'No existe la fecha'])],404);
        }
        //return response()->json(['status'=>'ok','data'=>$ia14],200);
        return new ShowResource($ia14);

    }

    public function showAll()
    {

        $ia14 = Ia14::all();
        if (! $ia14)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$ia14],200);
    }

    public function store(Request $request)
    {
        $ia14 = new Ia14();
        $ia14->fecha = $request->fecha;
        $ia14->ccaa_id = $request->ccaa_id;
        $ia14->media = $request->media;
        $ia14->save();
        return response()->json($ia14);
    }

    public function showCollection($id,$id2)
    {
        if ($id>$id2){
            return response()->json(['errors' =>Array(['code' => 404, 'message'=>'Primera fecha superior a la segunda'])],404);
        }
        $ia14 = DB::select(DB::raw("SELECT * FROM ia14 WHERE fecha BETWEEN '$id' and '$id2'"));
        //$ia14 = DB::select(DB::raw("select * from ia14 where fecha ='$id'"));
        if (!$ia14){
            return response()->json(['errors' =>Array(['code' => 404, 'message'=>'No existe la fecha'])],404);
        }
        //return response()->json(['status'=>'ok','data'=>$ia14],200);
        return new CovidCollection($ia14);

    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
