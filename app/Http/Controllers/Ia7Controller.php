<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Http\Resources\ShowResource;
use App\Models\Ia7;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Ia7Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ia7 = Ia7::where('fecha',$id)->first();
        //$ia14 = DB::select(DB::raw("select * from ia7 where fecha ='$id'"));
        if (!$ia7){
            return response()->json(['errors' =>Array(['code' => 404, 'message'=>'No existe la fecha'])],404);
        }
        //return response()->json(['status'=>'ok','data'=>$ia7],200);
        return new ShowResource($ia7);
    }

    public function showAll()
    {

        $ia7 = Ia7::all();
        if (! $ia7)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$ia7],200);
    }

    public function store(Request $request)
    {
        $ia7 = new Ia7();
        $ia7->fecha = $request->fecha;
        $ia7->ccaa_id = $request->ccaa_id;
        $ia7->media = $request->media;
        $ia7->save();
        return response()->json($ia7);
    }

    public function showCollection($id,$id2)
    {
        if ($id>$id2){
            return response()->json(['errors' =>Array(['code' => 404, 'message'=>'Primera fecha superior a la segunda'])],404);
        }
        $ia7 = DB::select(DB::raw("SELECT * FROM ia7 WHERE fecha BETWEEN '$id' and '$id2'"));
        //$ia14 = DB::select(DB::raw("select * from ia14 where fecha ='$id'"));
        if (!$ia7){
            return response()->json(['errors' =>Array(['code' => 404, 'message'=>'No existe la fecha'])],404);
        }
        //return response()->json(['status'=>'ok','data'=>$ia14],200);
        return new CovidCollection($ia7);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
