<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Casos extends Model
{
    use HasFactory;
    protected $table = 'casos';
    public $timestamps = false;
    protected $fillable = ['fecha','ccaas_id','numero'];
}
